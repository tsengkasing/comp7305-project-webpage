const path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader'
                },
                exclude: /node_modules/
            },
            {
                test: /\.styl$/,
                use: [
                    { loader: 'style-loader' },
                    {
                        loader: 'css-loader',
                        options: {
                            url: false,
                        },
                    },
                    { loader: 'stylus-loader' }
                ]
            }
        ]
    },
    devServer: {
        contentBase: [path.join(__dirname, 'public'), path.join(__dirname, 'dist')],
        compress: true,
        port: 9000,
        publicPath: '/dist/',
        hot: true,
        inline: true,
    }
};
